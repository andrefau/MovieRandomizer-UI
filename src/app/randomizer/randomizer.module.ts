import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { DxButtonModule, DxFormModule } from 'devextreme-angular';

import { RandomizerComponent } from './randomizer.component';

const randomizerRoutes: Routes = [
    { path: '', component: RandomizerComponent }
];

export const randomizerRouting = RouterModule.forChild(randomizerRoutes);

@NgModule({
  imports: [
    CommonModule,
    randomizerRouting,
    DxButtonModule,
    DxFormModule
  ],
  declarations: [ RandomizerComponent ],
  providers: []
})
export class RandomizerModule { }
