import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Movie } from '../core/movie.model';
import { DatabaseService } from '../core/database.service';

@Component({
  selector: 'app-randomizer',
  templateUrl: './randomizer.component.html',
  styleUrls: ['./randomizer.component.css']
})
export class RandomizerComponent implements OnInit, OnDestroy {
  movies: Movie[];
  chosenMovie: Movie;
  sub: any;
  isPaused = false;
  buttonTitle = 'Stop';
  showForm = false;

  constructor(private databaseService: DatabaseService) { }

  ngOnInit() {
    this.databaseService.getMovies()
      .then(response => {
        this.movies = response.data.filter(x => x.Available && !x.WatchedDate);
        this.sub = this.setFilm().subscribe(x => this.chosenMovie = x);
      });
  }

  setFilm() {
    return Observable
      .interval(100)
      .map(() => {
          return this.movies[Math.floor((Math.random() * this.movies.length))];
      });
  }

  click() {
    if (this.isPaused) {
      this.resetIntervall();
      this.isPaused = false;
      this.buttonTitle = 'Stop';
    } else {
      this.stopInterval();
      this.isPaused = true;
      this.buttonTitle = 'Start';
    }
  }

  stopInterval() {
    const start = 3;
    Observable
      .timer(0, 1000)
      .map(i => start - i)
      .take(start + 1)
      .subscribe(
        i => console.log(i),            // onNext
        e => console.log(e.message),    // onError
        () => {                         // onCompleted
          this.sub.unsubscribe();
          this.showForm = true;
        });
  }

  resetIntervall() {
    this.showForm = false;
    this.sub = this.setFilm().subscribe(x => this.chosenMovie = x);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
