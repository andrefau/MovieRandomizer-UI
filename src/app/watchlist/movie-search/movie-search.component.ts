import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MovieSearchService } from './movie-search.service';
import { Movie } from '../../core/movie.model';
import { DatabaseService } from '../../core/database.service';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.css'],
  providers: [ MovieSearchService ]
})
export class MovieSearchComponent implements OnInit {
  selectedMovie: any;
  dataSource: any = {};
  dataSourceId: any = {};

  @Output()
  update: EventEmitter<string> = new EventEmitter();

  constructor(private movieSearchService: MovieSearchService, private databaseService: DatabaseService) { }

  ngOnInit() {
    this.setupDataSource();
  }

  setupDataSource() {
    this.dataSource.store = new CustomStore({
      load: (loadOptions) => {
        return this.movieSearchService.search(loadOptions.searchValue);
      }
    });

    this.dataSourceId.store = new CustomStore({
      load: (loadOptions) => {
        return this.movieSearchService.searchById(loadOptions.searchValue);
      }
    });
  }

  addMovie() {
    if (!this.selectedMovie.imdbID) {
      notify('Not a valid movie', 'info', 2000);
      return;
    }

    this.movieSearchService.getMovieDetails(this.selectedMovie.imdbID)
      .then(details => {

        this.databaseService.postMovie(this.selectedMovie.imdbID, details.data)
          .then(data => {
            this.update.emit('update');
          });
      });
  }

  convertDetailsToMovie(details) {
    const regex = /\d+/g; // Runtime kommer på formatet '120 min'. Bruker regex for å hente tallet

    const title = (details.Title !== 'N/A') ? details.Title : null;
    const released = (details.Released !== 'N/A') ? details.Released : null;
    const genre = (details.Genre !== 'N/A') ? details.Genre : null;
    const runtime = (details.Runtime !== 'N/A') ? details.Runtime.match(regex)[0] : null;
    const metascore = (details.Metascore !== 'N/A') ? details.Metascore : null;
    const imdbRating = (details.imdbRating !== 'N/A') ? details.imdbRating : null;
    const link = (details.imdbID !== 'N/A') ? details.imdbID : null;

    const movie = new Movie(
      title,
      released,
      genre,
      runtime,
      metascore,
      imdbRating,
      null,
      null,
      link,
      null
    );

    return movie;
  }

}
