import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Movie } from '../../core/movie.model';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class MovieSearchService {

  constructor(private http: Http) { }

  search(title: string) {
    const encoded = encodeURIComponent(title);
    const url = 'https://www.omdbapi.com/?s=' + encoded + '&type=movie&apikey=' + environment.apiKey;
    return this.http.get(url)
      .toPromise()
      .then(response => {
          const json = response.json();
          const res: any = (json.Response === 'True') ? json.Search : { Title: 'Movie not found!' };
          return {
              data: res
          };
      })
      .catch(error => { throw error; });
  }

  searchById(id: string) {
    const url = 'https://www.omdbapi.com/?i=' + id + '&type=movie&apikey=' + environment.apiKey;
    return this.http.get(url)
      .toPromise()
      .then(response => {
          const json = response.json();
          return {
              data: json
          };
      })
      .catch(error => { throw error; });
  }

  getMovieDetails(imdbId) {
    const url = 'https://www.omdbapi.com/?i=' + imdbId + '&apikey=' + environment.apiKey;
    return this.http.get(url)
      .toPromise()
      .then(response => {
          const json = response.json();
          return {
              data: json
          };
      })
      .catch(error => { throw error; });
  }

}
