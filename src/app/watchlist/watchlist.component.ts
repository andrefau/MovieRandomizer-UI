import { Component, OnInit, ViewChild } from '@angular/core';

import { DatabaseService } from '../core/database.service';
import { Movie } from '../core/movie.model';
import { DxDataGridComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})
export class WatchlistComponent implements OnInit {
  @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
  dataSource: any = {};

  constructor(private databaseService: DatabaseService) { }

  ngOnInit() {
    this.setupDataSource();
  }

  setupDataSource() {
    this.dataSource.store = new CustomStore({
        load: (loadOptions) => {
          return this.databaseService.getMovies();
        },

        insert: (value) => {
          console.log('Insert is not implemented');
          return null;
        },

        update: (key, value) => {
          return this.databaseService.updateMovie(key.imdbID, value);
        },

        remove: (movie) => {
          return this.databaseService.deleteMovie(movie);
        }
      });
  }

  updateMovie(movie: Movie, object: any) {
    if (object.Title) { movie.Title = object.Title; }
    if (object.ReleaseDate) { movie.ReleaseDate = object.ReleaseDate; }
    if (object.Genre) { movie.Genre = object.Genre; }
    if (object.Runtime) { movie.Runtime = object.Runtime; }
    if (object.Metascore) { movie.Metascore = object.Metascore; }
    if (object.ImdbScore) { movie.ImdbScore = object.ImdbScore; }
    if (object.MyScore) { movie.MyScore = object.MyScore; }
    if (object.WatchedDate) { movie.WatchedDate = object.WatchedDate; }
    if (object.Link) { movie.Link = object.Link; }
    if (object.Available) { movie.Available = object.Available; }
  }

  updateGrid() {
    this.dataGrid.instance.refresh();
  }

}
