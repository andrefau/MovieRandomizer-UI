import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { DxButtonModule,
         DxDataGridModule,
         DxAutocompleteModule } from 'devextreme-angular';

import { WatchlistComponent } from './watchlist.component';
import { MovieSearchComponent } from './movie-search/movie-search.component';

const watchlistRoutes: Routes = [
  { path: '', component: WatchlistComponent }
];

export const watchlistRouting = RouterModule.forChild(watchlistRoutes);

@NgModule({
  imports: [
    CommonModule,
    watchlistRouting,
    DxButtonModule,
    DxDataGridModule,
    DxAutocompleteModule
  ],
  declarations: [
    WatchlistComponent,
    MovieSearchComponent
  ]
})
export class WatchlistModule { }
