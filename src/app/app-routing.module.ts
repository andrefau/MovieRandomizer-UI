import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './core/auth-guard';

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'watchlist', loadChildren: 'app/watchlist/watchlist.module#WatchlistModule', canActivate: [AuthGuard] },
    { path: 'randomizer', loadChildren: 'app/randomizer/randomizer.module#RandomizerModule', canActivate: [AuthGuard] },
    {
        path: '',
        redirectTo: '/watchlist',
        pathMatch: 'full',
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: 'watchlist',
        canActivate: [AuthGuard]
    }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
