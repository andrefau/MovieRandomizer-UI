import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AuthenticationService } from './authentication.service';
import { AuthGuard } from './auth-guard';
import { DatabaseService } from './database.service';

export const firebaseConfig = {
    apiKey: 'AIzaSyAjH73E4UdyevOHM2I2zWePX9twUs0YDgY',
    authDomain: 'watchlist-3fdbb.firebaseapp.com',
    databaseURL: 'https://watchlist-3fdbb.firebaseio.com',
    projectId: 'watchlist-3fdbb',
    storageBucket: 'watchlist-3fdbb.appspot.com',
    messagingSenderId: '181801321859'
  };

@NgModule({
  imports: [
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    CommonModule
  ],
  declarations: [],
  providers: [ AuthenticationService, AuthGuard, DatabaseService ]
})
export class CoreModule { }
