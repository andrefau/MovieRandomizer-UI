import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Injectable()
export class DatabaseService {

  items: FirebaseListObservable<any[]>;

  constructor(private db: AngularFireDatabase) {
    this.items = db.list('movies');
  }

  getMovies() {
    return this.db.list('movies', {
      query: {
        orderByChild: 'ReleaseDate'
      }
    })
    .elementAt(0).toPromise()
    .then(response => {
      return {
        data: response,
        totalCount: response.length
      };
    })
    .catch(error => {
      throw error;
    });
  }

  deleteMovie(movie: any) {
    return this.items.remove(movie.imdbID)
      .then(response => {
        return response;
      })
      .catch(error => {
        throw error;
      });
  }

  postMovie(id, movie) {
    return this.db.database.ref('movies/' + id).set(movie)
      .then(response => {
        return response;
      })
      .catch(error => {
        throw error;
      });
  }

  updateMovie(id, movie) {
    return this.items.update(id, movie)
    .then(response => {
        return response;
      })
      .catch(error => {
        throw error;
      });
  }

}
