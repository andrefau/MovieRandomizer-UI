export class Movie {
    static parseArray(json: any[]): Movie[] {
        return json.map(element => new Movie(
            element.Title,
            element.Released,
            element.Genre,
            element.Runtime,
            element.Metascore,
            element.imdbRating,
            element.MyScore,
            element.WatchedDate,
            element.Link,
            element.Available
        ));
    }

    static parseObject(json: any): Movie {
        return new Movie(
            json.Title,
            json.Released,
            json.Genre,
            json.Runtime,
            json.Metascore,
            json.imdbRating,
            json.MyScore,
            json.WatchedDate,
            json.Link,
            json.Available
        );
    };

    constructor(
        public Title: string,
        public ReleaseDate: Date,
        public Genre: string,
        public Runtime: number,
        public Metascore: number,
        public ImdbScore: number,
        public MyScore: number,
        public WatchedDate: Date,
        public Link: string,
        public Available: string
    ) { }
}
