import { MovieRandomizerPage } from './app.po';

describe('movie-randomizer App', () => {
  let page: MovieRandomizerPage;

  beforeEach(() => {
    page = new MovieRandomizerPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
